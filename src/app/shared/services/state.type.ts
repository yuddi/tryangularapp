export interface IPaginationDefault {
  currentPage?: number;
  limitPerPage?: number;
  searchValue?: string;
  order?: string;
  sort?: string;
}

export interface IEmployeeState {
  list: IPaginationDefault;
}
