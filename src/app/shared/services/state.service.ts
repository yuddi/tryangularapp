import { Injectable } from '@angular/core';
import { IEmployeeState } from './state.type';

@Injectable({
  providedIn: 'root',
})
export class StateService {
  public employeeState: IEmployeeState;

  constructor() {
    this.employeeState = {
      list: {
        currentPage: 1,
        limitPerPage: 10
      },
    };
  }
}
