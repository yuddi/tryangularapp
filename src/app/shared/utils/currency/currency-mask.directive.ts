import { Directive, HostListener, Input, Optional, Self } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  standalone: true,
  selector: '[appCurrencyMask]',
})
export class CurrencyMaskDirective {
  @Input() appCurrencyMask: string = '';
  constructor(@Self() private readonly ngControl: NgControl) {}

  @HostListener('ngModelChange', ['$event'])
  onModelChange(event: any) {
    this.onInputChange(event, false);
  }

  @HostListener('keydown.backspace', ['$event'])
  keydownBackspace(event: { target: { value: any } }) {
    this.onInputChange(event.target.value, true);
  }

  onInputChange(event: { toString: () => string }, backspace: boolean) {
    let newVal: string = event?.toString().replace(/\D/g, '') || '';
    if (backspace) {
      newVal = newVal.substring(0, newVal.length);
    }
    if (newVal.length === 0) {
      newVal = '';
      this.ngControl?.valueAccessor?.writeValue(newVal);
    } else {
      newVal = newVal
        .replace('.', ',')
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
      this.ngControl?.valueAccessor?.writeValue(
        `${this.appCurrencyMask}${newVal}`
      );
    }
    this.ngControl.control?.setValue(this.toNumber(newVal), {
      emitEvent: false,
      emitModelToViewChange: false,
      emitViewToModelChange: false,
    });
  }

  toNumber(val: any): number {
    const rValue = val.replace(/\D/g, '');
    return rValue as number;
  }
}
