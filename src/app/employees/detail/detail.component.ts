import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { dataEmployees } from '../../shared/mocks/employees.mock';
import { IEmployee, TEmployees } from '../employees.type';
import { CommonModule, Location, registerLocaleData } from '@angular/common';
import localeId from '@angular/common/locales/id';
registerLocaleData(localeId, 'id');

@Component({
  selector: 'app-detail',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './detail.component.html',
  styleUrl: './detail.component.scss',
})
export class DetailComponent {
  detailData: IEmployee | null;

  constructor(private route: ActivatedRoute, private location: Location) {
    this.detailData = null;
    this.route.params.subscribe((params: any) => {
      if (params?.id) {
        this.getDetailById(params?.id);
      }
    });
  }

  getDetailById(id: string) {
    let dataStorage: TEmployees = JSON.parse(
      localStorage.getItem('DATA_EMPLOYEE') || '[]'
    );
    if (!dataStorage?.length) {
      dataStorage = dataEmployees;
      localStorage.setItem('DATA_EMPLOYEE', JSON.stringify(dataEmployees));
    }
    this.detailData = dataStorage.find((item) => item.username === id) || null;
  }

  onOK() {
    this.location.back();
  }
}
