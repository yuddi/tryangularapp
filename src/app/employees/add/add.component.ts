import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { provideNativeDateAdapter } from '@angular/material/core';
import { CommonModule, Location } from '@angular/common';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { CurrencyMaskDirective } from '../../shared/utils/currency/currency-mask.directive';
import { dataGroup } from '../../shared/mocks/master-data.mock';
import { provideMomentDateAdapter } from '@angular/material-moment-adapter';
import { Router } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';
import { TEmployees } from '../employees.type';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD MMMM YYYY',
  },
  display: {
    dateInput: 'DD MMMM YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-add',
  standalone: true,
  providers: [provideNativeDateAdapter(), provideMomentDateAdapter(MY_FORMATS)],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatButtonModule,
    CurrencyMaskDirective,
    FormsModule,
    ReactiveFormsModule,
  ],
  templateUrl: './add.component.html',
  styleUrl: './add.component.scss',
})
export class AddComponent {
  maxDate = new Date();
  groupOptions = dataGroup;
  groupSelected: string = '';
  addEmployeeForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private location: Location,
    private router: Router,
    private toast: NgToastService
  ) {
    this.addEmployeeForm = this.formBuilder.group({});
  }

  ngOnInit() {
    this.addEmployeeForm = this.formBuilder.group({
      username: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      birthDate: ['', Validators.required],
      basicSalary: ['', Validators.required],
      status: ['', Validators.required],
      group: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  onCancel() {
    this.location.back();
  }

  onSave() {
    if (this.addEmployeeForm.valid) {
      let dataStorage: TEmployees = JSON.parse(
        localStorage.getItem('DATA_EMPLOYEE') || '[]'
      );
      const rawData = this.addEmployeeForm.getRawValue();
      const tempData = [
        { ...rawData, birthDate: new Date(rawData.birthDate) },
        ...dataStorage,
      ];
      localStorage.setItem('DATA_EMPLOYEE', JSON.stringify(tempData));
      this.location.back();
      this.router.navigate(['employee'], { replaceUrl: true });
    } else {
      this.addEmployeeForm.markAllAsTouched();
      this.toast.error({
        detail: `Warning (Invalid Form Data)`,
        summary: `Fill in the required fields correctly`,
        duration: 3000,
      });
    }
  }

  checkRequired(val: string) {
    return (
      this.addEmployeeForm.get(val)?.touched &&
      !this.addEmployeeForm.get(val)?.valid
    );
  }
}
