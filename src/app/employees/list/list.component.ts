import { Component, OnDestroy, OnInit } from '@angular/core';
import { dataEmployees } from '../../shared/mocks/employees.mock';
import { IEmployee, TDefaultMasterData, TEmployees } from '../employees.type';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { Subject, debounceTime } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { NgToastModule, NgToastService } from 'ng-angular-popup';
import { StateService } from '../../shared/services/state.service';
import { limitPerPageOptions } from '../../shared/mocks/master-data.mock';

const DEBOUNCE_TIME = 1000;

@Component({
  selector: 'app-list',
  standalone: true,
  imports: [CommonModule, FormsModule, NgToastModule],
  templateUrl: './list.component.html',
  styleUrl: './list.component.scss',
})
export class ListComponent implements OnInit, OnDestroy {
  dataEmployees: TEmployees = [];
  isLoading: boolean;
  searchText: string;
  currentPage: number;
  totalPage: number;
  limitPerPage: number;
  isPrevPage: boolean;
  isNextPage: boolean;
  limitPerPageOptions: TDefaultMasterData[];
  filteredData: TEmployees = [];
  isSorting: 'asc' | 'desc' | null;

  private searchSubject = new Subject<string>();

  constructor(
    private router: Router,
    private toast: NgToastService,
    private state: StateService
  ) {
    this.limitPerPageOptions = limitPerPageOptions;
    this.dataEmployees = [];
    this.isLoading = false;
    this.currentPage = this.getState().currentPage || 1;
    this.totalPage = 0;
    this.limitPerPage = this.getState().limitPerPage || 10;
    this.isPrevPage = true;
    this.isNextPage = true;
    this.searchText = this.getState().searchValue || '';
    this.isSorting = null;
  }

  getState() {
    return this.state.employeeState.list;
  }

  setState(updatedValue: typeof this.state.employeeState.list) {
    this.state.employeeState.list = {
      ...this.state.employeeState.list,
      ...updatedValue,
    };
  }

  ngOnInit(): void {
    this.searchSubject
      .pipe(debounceTime(DEBOUNCE_TIME))
      .subscribe((searchValue) => {
        this.actionSearch(searchValue);
      });

    const start = (this.currentPage - 1) * this.limitPerPage;
    const end = (this.currentPage - 1) * this.limitPerPage + this.limitPerPage;
    this.getList(this.searchText, start, end);
  }

  onSearch(e: Event) {
    const target = e.target as HTMLInputElement;
    if (target) {
      this.searchSubject.next(target?.value);
    }
  }

  actionSearch(value: string) {
    this.currentPage = 1;
    this.getList(value);
  }

  onFilteredListData(searchValue?: string) {
    let dataStorage: TEmployees = JSON.parse(
      localStorage.getItem('DATA_EMPLOYEE') || '[]'
    );
    if (!dataStorage?.length) {
      dataStorage = dataEmployees;
      localStorage.setItem('DATA_EMPLOYEE', JSON.stringify(dataEmployees));
    }
    const filteredData = dataStorage.filter(
      (item) =>
        item.username.search(searchValue as string) !== -1 ||
        `${item.firstName} ${item.lastName}`.search(searchValue as string) !==
          -1
    );
    this.totalPage = Math.ceil(filteredData.length / this.limitPerPage);
    this.filteredData = filteredData;
    return filteredData;
  }

  onSortBy(param: string) {
    function compareASC(a: any, b: any) {
      if (a[param] < b[param]) {
        return -1;
      }
      if (a[param] > b[param]) {
        return 1;
      }
      return 0;
    }

    function compareDESC(a: any, b: any) {
      if (a[param] > b[param]) {
        return -1;
      }
      if (a[param] < b[param]) {
        return 1;
      }
      return 0;
    }
    switch (this.isSorting) {
      case 'asc':
        const hasDesc = [...this.filteredData].sort(compareDESC);
        this.dataEmployees = hasDesc;
        this.isSorting = 'desc';
        break;
      case 'desc':
        this.dataEmployees = [...this.filteredData];
        this.isSorting = null;
        break;
      default:
        const hasAsc = [...this.filteredData].sort(compareASC);
        this.dataEmployees = hasAsc;
        this.isSorting = 'asc';
        break;
    }
  }

  getList(
    searchValue?: string,
    start = 0,
    end = this.limitPerPage
  ): TEmployees | any {
    this.isLoading = true;
    return new Promise<TEmployees>((resolve, reject) => {
      setTimeout(() => {
        try {
          const filteredData = this.onFilteredListData(searchValue);
          this.dataEmployees = filteredData.slice(start, end);
          this.setState({ searchValue });
          this.isLoading = false;
          resolve(this.dataEmployees);
        } catch (x) {
          this.isLoading = false;
          reject(x);
        }
      }, 2000);
    });
  }

  onNavToAdd() {
    this.router.navigate([`employee/add`]);
  }

  onNavToDetail(item: IEmployee) {
    this.router.navigate([`employee/${item.username}`]);
  }

  onPageLimitChange(e: Event) {
    if ((e.target as HTMLSelectElement).value) {
      this.limitPerPage = Number(
        (e.target as HTMLSelectElement).value || this.limitPerPage
      );
      this.dataEmployees = this.filteredData.slice(0, this.limitPerPage);
      this.setState({
        limitPerPage: this.limitPerPage,
      });
      this.currentPage = 1;
      const filteredData = this.onFilteredListData();
      this.totalPage = Math.ceil(filteredData.length / this.limitPerPage);
      this.isNextPage = this.currentPage !== this.totalPage;
    }
  }

  onEdit(item: IEmployee) {
    this.toast.success({
      detail: `You Click Edit`,
      summary: `${item.username} - ${item.firstName} ${item.lastName}`,
      duration: 2000,
    });
  }

  onDelete(item: IEmployee) {
    this.toast.error({
      detail: `You Click Delete `,
      summary: `${item.username} - ${item.firstName} ${item.lastName}`,
      duration: 2000,
    });
  }

  async onPrevious() {
    this.totalPage = Math.ceil(this.filteredData.length / this.limitPerPage);

    if (this.currentPage > 1) {
      const searchValue = this.getState().searchValue;
      const start = (this.currentPage - 2) * this.limitPerPage;
      const end =
        (this.currentPage - 2) * this.limitPerPage + this.limitPerPage;
      this.dataEmployees = await this.getList(searchValue, start, end);
      const prevPage = (this.currentPage = Number(this.currentPage) - 1);
      this.setState({
        currentPage: prevPage,
      });
      this.isNextPage = true;
      if (prevPage === 1) {
        this.isPrevPage = false;
      }
    } else {
      this.isPrevPage = false;
    }
  }

  async onNext() {
    this.totalPage = Math.ceil(this.filteredData.length / this.limitPerPage);

    if (this.currentPage !== this.totalPage) {
      const searchValue = this.getState().searchValue;
      const start = this.currentPage * this.limitPerPage;
      const end = this.currentPage * this.limitPerPage + this.limitPerPage;
      this.dataEmployees = await this.getList(searchValue, start, end);
      const nextPage = (this.currentPage = Number(this.currentPage) + 1);
      this.setState({
        currentPage: nextPage,
      });
      this.isPrevPage = true;
      if (nextPage === this.totalPage) {
        this.isNextPage = false;
      }
    } else {
      this.isNextPage = false;
    }
  }

  ngOnDestroy(): void {
    this.searchSubject.complete();
  }
}
