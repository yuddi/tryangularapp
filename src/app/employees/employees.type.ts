export interface IEmployee {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: Date;
  basicSalary: number;
  status: string;
  group: string;
  description: string;
}

export type TEmployees = IEmployee[];

export type TDefaultMasterData = {
  label: string;
  value: string | number;
};
