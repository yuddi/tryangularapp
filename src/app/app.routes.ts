import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';

const employeesModule = () =>
  import('./employees/employees.module').then((m) => m.EmployeesModule);

export const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'employee',
    loadChildren: employeesModule,
  },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
];
