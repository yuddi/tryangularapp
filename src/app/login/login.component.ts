import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss',
})
export class LoginComponent {
  username: string = 'admin';
  password: string = 'admin';

  constructor(private router: Router, private toast: NgToastService) {}

  onSignIn() {
    if (this.username === 'admin' && this.password === 'admin') {
      this.router.navigate(['employee']);
    } else {
      this.toast.warning({
        detail: `Warning`,
        summary: `Username or Password mismatch`,
        duration: 3000,
      });
    }
  }
}
